# История изменений
В этом файле содержится описание всех значимых изменений продукта.
Маркировка версий ведётся в соответствии с [Semantic Versioning](http://semver.org/).

## [2.0.0] - 2016-XX-XX
### Новый функционал
- Переделан способ поставки хуков: один плагин будет содержать несколько хуков, вместо схемы один плагин-один хук, которая использовалась ранее.
- Добавлен хук проверки исходных кодов на соответствие pep257.

### Правки
- Добавлено игнорирование папки south_migrations - для совместимости с Django 1.8.


## [1.1.0] - 2015-11-09
### Новый функционал
- Реализован выбор пользователя, от имени которого будут прикрепляться комментарии к PR.

### Правки
- Исправлен ещё один случай, когда комменты добавлялись на файлы, которых нет в диффе PR.


## [1.0.1] - 2015-08-06
### Правки
- Комменты больше не добавляются на файлы, которых нет в диффе PR.
- Папки с миграциями заигнорированы.
- Корректная обработка выключателей на уровне репозитория - плагин работает теперь только там, где включен.


[2.0.0]: https://bitbucket.org/ivansu/python-linters/branches/compare/cd0bd9c%0D1.1.0
[1.1.0]: https://bitbucket.org/ivansu/python-linters/branches/compare/9bc2b28%0D1.0.1
[1.0.1]: https://bitbucket.org/ivansu/python-linters/branches/compare/f131e7d%0D1.0.0
