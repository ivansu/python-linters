package com.bars.stash.hook.checks;

import java.io.*;
import java.lang.String;

import com.atlassian.stash.hook.repository.RepositoryHookService;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.UserService;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.PermissionService;

/**
 * Класс, реализующий проверку на pep257.
 *
 * @author Судаков Иван <ivan.s.sudakov@gmail.com>
 * @since  27.11.15
 */
public class Pep257Hook extends AbstractHook {

    public Pep257Hook(PullRequestService pullRequestService, ApplicationPropertiesService applicationPropertiesService,
                      RepositoryHookService repositoryHookService, UserService userService, SecurityService securityService,
                      PermissionService permissionService) {
        this.pullRequestService = pullRequestService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.repositoryHookService = repositoryHookService;
        this.userService = userService;
        this.securityService = securityService;
        this.permissionService = permissionService;

        hookKey = "com.bars.python-linters:pep257";
        homeDir = System.getProperty("stash.home");
        hookDir = homeDir + "/external-hooks/pep257";
    }

    protected void doCheck(PullRequest pr) {

        repository = pr.getToRef().getRepository();

        if (!repositoryHookService.getByKey(repository, hookKey).isEnabled()) {
            return;
        }

        String fromRef = pr.getFromRef().getLatestChangeset();
        String toRef = pr.getToRef().getLatestChangeset();

        File repositoryDirObject = applicationPropertiesService.getRepositoryDir(repository);

        String s = null;

        try {
            //Запускаем команду
            String cmd = this.hookDir + "/runner_pep257.sh " + this.hookDir + "/pep257.py " + toRef + " " + fromRef;
            String[] envp = {};

            Process p = Runtime.getRuntime().exec(cmd, envp, repositoryDirObject);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            // Нашли предупреждение линтёра
            while ((s = stdInput.readLine()) != null) {

                String[] parts = s.split(":");
                String[] parts2 = parts[1].split(" ");

                final String srcPath = parts[0].substring(2);
                final int line = Integer.parseInt(parts2[0]);
                final String level = parts[1].substring(parts2[0].length()).trim();
                final String comment = "Нарушение PEP257 (" + level + "): " + parts[2].trim() + "  " + parts[3].trim();

                addComment(repository, pr, srcPath, line, comment);
            }

        } catch (IOException e) {
            log.error("Error running pep257 checker");
        }
    }
}
