package com.bars.stash.hook.checks;

import java.lang.String;
import java.util.Collection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.atlassian.event.api.EventListener;
import com.atlassian.stash.content.DiffFileType;
import com.atlassian.stash.content.DiffSegmentType;
import com.atlassian.stash.event.pull.PullRequestOpenedEvent;
import com.atlassian.stash.event.pull.PullRequestRescopedEvent;
import com.atlassian.stash.event.pull.PullRequestReopenedEvent;
import com.atlassian.stash.event.pull.PullRequestUpdatedEvent;
import com.atlassian.stash.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.hook.repository.RepositoryHookService;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.atlassian.stash.util.Operation;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionService;


/**
 * Класс, реализующий общую логику всех хуков.
 *
 * @author Судаков Иван <ivan.s.sudakov@gmail.com>
 * @since  27.11.15
 */
abstract public class AbstractHook implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator {

    protected PullRequestService pullRequestService;
    protected ApplicationPropertiesService applicationPropertiesService;
    protected RepositoryHookService repositoryHookService;
    protected UserService userService;
    protected SecurityService securityService;
    protected PermissionService permissionService;

    protected static final Logger log = LogManager.getLogger("atlassian.plugin");

    protected String hookKey;
    protected String hookDir;
    protected String homeDir;
    protected Repository repository;

    @Override
    public void validate(Settings settings, SettingsValidationErrors errors, Repository repository) {
        if (settings.getString("username", "").isEmpty()) {
            errors.addFieldError("username", "Пожалуйста, введите логин пользователя.");
            return;
        }

        StashUser user = userService.getUserByName(settings.getString("username").trim());

        if (user == null) {
            errors.addFieldError("username", "Пользователь не найден.");
            return;
        }
        if (!permissionService.hasRepositoryPermission(user, repository, Permission.REPO_READ)) {
            errors.addFieldError("username", "Пользователь \"" + user.getDisplayName() + "\" не имеет прав на чтение репозитория. Пожалуйста, настройте права (должны быть, как минимум, на чтение) или выберите другого пользователя.");
            return;
        }
    }

    public void postReceive(RepositoryHookContext context, Collection<RefChange> refChanges) {}

    @EventListener
    public void pullRequestOpened(PullRequestOpenedEvent prEvent)
    {
        doCheck(prEvent.getPullRequest());
    }

    @EventListener
    public void pullRequestRescoped(PullRequestRescopedEvent prEvent)
    {
        doCheck(prEvent.getPullRequest());
    }

    @EventListener
    public void pullRequestReopened(PullRequestReopenedEvent prEvent)
    {
        doCheck(prEvent.getPullRequest());
    }

    @EventListener
    public void pullRequestUpdated(PullRequestUpdatedEvent prEvent)
    {
        doCheck(prEvent.getPullRequest());
    }

    /**
     * Выполняет проверку PR и комментирует.
     *
     * @param pr
     */
    abstract protected void doCheck(PullRequest pr);

    /**
     * Добавляет комментарий к PR.
     *
     * @param repository    Репозиторий
     * @param pullRequest   Пулл-реквест
     * @param srcPath       Путь до файла
     * @param line          Номер строки
     * @param comment       Текст комментария
     * @return
     */
    protected void addComment(final Repository repository, final PullRequest pullRequest, final String srcPath, final int line, final String comment) {

        // Оставляем комментарий, от определённого пользователя.
        securityService.impersonating(
                getCommenter(),
                "Чтобы никого не обидеть авторством комментария."
        ).call(new Operation<Object, RuntimeException>() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public Object perform() {
                com.atlassian.stash.comment.AddDiffCommentRequest.Builder addDiffCommentRequestBuilder = new com.atlassian.stash.comment.AddDiffCommentRequest.Builder();

                return pullRequestService.addDiffComment(
                        repository.getId(),
                        pullRequest.getId(),
                        addDiffCommentRequestBuilder.path(srcPath).fileType(DiffFileType.TO).lineType(DiffSegmentType.ADDED).line(line).text(comment).build()
                );
            }
        });
    }

    /**
     * Возвращает пользователя, от имени которого будут комменты.
     * Пользователь должен иметь, как минимум, права на чтение репозитория.
     *
     * @return StashUser
     */
    protected StashUser getCommenter() {
        return userService.getUserByName(repositoryHookService.getSettings(repository, hookKey).getString("username").trim());
    }
}
